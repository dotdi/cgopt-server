#include <iostream>
#include "MessageQueue.h"

int main() {
    std::cout << "Hello, World!" << std::endl;

    MessageQueue msgQueue;
    msgQueue.serve_pubsub("*", 5678);

    return 0;
}