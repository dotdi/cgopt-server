//
// Created by davidsere on 15.06.15.
//

#ifndef CGOPT_SERVER_MESSAGEQUEUE_H
#define CGOPT_SERVER_MESSAGEQUEUE_H

#include <string>
#include <queue>
#include "cgopt.pb.h"

class MessageQueue {
public:
    void serve_pubsub(std::string host, int port);
private:
    bool continueRunning = true;
    void debug_request(cgopt_request request);
    void debug_response(cgopt_response response);
    cgopt_response process_req(cgopt_request request,
                               std::queue<cgopt_response> &  worker_queue,
                               std::queue<cgopt_response> &  server_queue);
};

#endif //CGOPT_SERVER_MESSAGEQUEUE_H
